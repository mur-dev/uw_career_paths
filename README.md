# University of Waterloo: Career paths

Stores and displays career paths for prospective students.

# Requirements

* [Drupal core](https://www.drupal.org/project/drupal), version 7.0 or higher
* [The Tablefield module](https://www.drupal.org/project/tablefield)
* The *D3 Sankey: Table grouping preprocessor* (`d3_sankey_table_group_pp`) sub-module inside [the D3 Sankey module](https://www.drupal.org/project/d3_sankey), which depends on:
    * [The Composer Manager contributed module](https://www.drupal.org/project/composer_manager),
    * [The D3 Sankey contributed module](https://www.drupal.org/project/d3_sankey), which depends on:
        * [The D3 contribtued module](https://www.drupal.org/project/d3)
        * [The Libraries API contributed module](https://www.drupal.org/project/libraries), version 7.x-2.0 or higher
        * [The XAutoload contributed module](https://www.drupal.org/project/xautoload), version 7.x-5.0 or higher
        * [The D3 JavaScript library](http://d3js.org/), [downloaded](https://github.com/d3/d3/releases/) into `sites/*/libraries`
        * [The d3-sankey library](https://github.com/newrelic-forks/d3-plugins-sankey), [downloaded](https://github.com/newrelic-forks/d3-plugins-sankey/releases) into `sites/*/libraries`
        * [The d3.chart library](https://github.com/misoproject/d3.chart), [downloaded](https://github.com/misoproject/d3.chart/releases) into `sites/*/libraries`
        * [The d3.chart.sankey library](https://github.com/q-m/d3.chart.sankey), [downloaded](https://github.com/q-m/d3.chart.sankey/releases) into `sites/*/libraries`

# Installation

1. Download, install, and enable this module and its dependencies. See https://drupal.org/node/895232 for further information.
2. Download and install all JavaScript libraries that this module and its dependencies need. See https://www.drupal.org/node/1440066 for further information.

# Configuration

1. Manage the fields for any content type.
    * If you have an existing Tablefield whose input form you want to turn into an upload field with the extra Sankey diagram overrides, edit the Tablefield, click the *Widget type* tab, and set the *Widget type* to `UW Career Paths: Tablefield (upload only)`.
    * To create a new Tablefield for entering Sankey data, under *Add new field*, enter a *Label*. Select the `Table Field` field type, and the `UW Career Paths: Tablefield (upload only)` form element to edit the data. Click `Save`. In the *Field settings* screen, you probably want to check `Hide table header row.` and set *Table cell processing* to `PLain text`. On the *Edit* screen, you probably want to set the *Number of values* to `1`. Click `Save settings` when you are done.
2. Manage the display for any content type.
    * If you have an existing Tablefield that you want to display as a Sankey diagram, set the *Format* for that field to `Sankey diagram (table grouping preprocessor)`. Then, click the gear, and set the appropriate display settings (the defaults work nicely on the Bartik theme).
